<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Hemos utilizado metodo magico constructor</h2>
</div>

<?php

// creamos una clase 
class Alumno
{
    // propiedades de la clase

    // visibilidad nombrePropiedad
    public ?string $nombre = null;
    public ?string $apellidos = null;
    public ?int $edad = 0;

    // metodos magicos de la clase
    // son metodos que se ejecutan automaticamente
    // ante determinadas condiciones
    public function __construct(?string $nombre = null, ?string $apellidos = null, ?int $edad = null)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->edad = $edad;
    }


    // metodos de la clase

    // visibilidad function nombreMetodo(argumentos)
    public function saludar()
    {
        return "Hola clase<br>";
    }

    public function presentacion()
    {
        return "hola mi nombre es {$this->nombre} y mis apellidos son {$this->apellidos}<br>";
    }
}
?>

<?php
// para poder utilizar la clase
// tengo que generar una instancia

// creamos un objeto de tipo Alumno
$alumno1 = new Alumno("Ana", "Vazquez", 40); // cuando creo el objeto se llama al constructor

// acceder al metodo presentacion
// para mostrarlo en pantalla

echo $alumno1->presentacion();

var_dump($alumno1);

// Creat otro objeto alumno

$alumno2 = new Alumno("Luis", "Gómez", 30);

var_dump($alumno2);

$alumno3 = new Alumno();

var_dump($alumno3);

$alumno4 = new Alumno("María");

var_dump($alumno4);
