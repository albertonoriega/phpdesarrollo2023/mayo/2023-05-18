<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>crear una clase denomina coche con las siguientes propiedades</h2>
    <li>tipo</li>
    <li>matricula</li>
    <li>cilindrada</li>
    <li>fechaCompra</li>

    <p>Crear los getter y setter para todas las propiedades (los setter fluent)</p>

    <p>Constructor para inicializar solo matricula</p>

    <p>Metodo toString para imprimir matricula y tipo en una lista</p>

</div>

<?php

// crear una clase denomina coche
// con las siguientes propiedades
// tipo
// matricula
// cilindrada
// fechaCompra
// Crear los getter y setter para todas las propiedades
// los setter fluent
// constructor para inicializar solo matricula
// metodo toString para imprimir matricula y tipo en una lista

class Coche
{
    public ?string $tipo;
    private ?string $matricula;
    public ?int $cilindrada;
    public ?string $fechaCompra;

    function __construct($matricula)
    {
        $this->matricula = $matricula;
    }

    //GETTERS
    public function getTipo()
    {
        return $this->tipo;
    }

    public function getCilindrada()
    {
        return $this->cilindrada;
    }

    public function getMatricula()
    {
        return $this->matricula;
    }
    public function fechaCompra()
    {
        return $this->fechaCompra();
    }
    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;

        return $this;
    }

    //SETTERS
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this; // para que sea fluent
    }
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
        return $this;
    }
    public function setCilindrada($cilindrada)
    {
        $this->cilindrada = $cilindrada;
        return $this;
    }


    // Método toString

    public function __toString()
    {
        return "<ul> <li> Tipo: {$this->tipo} </li> <li> Matricula: {$this->matricula}</li> </ul>";
    }
}

$coche1 = new Coche('5896HGT');

$coche1->setTipo('Turismo');
$coche1->setFechaCompra('2023-04-11');
$coche1->setCilindrada(40);

var_dump($coche1);

echo $coche1;
