<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <h2>Vamos a utilizar los espacios de nombres en las clases y la importancion de estas mediante use</h2>
    <p>Vamos a utilizar la clase Coche en el espacio clases/objetos/Coche. Esta clase la vamos a importar primero</p>
    <p>Vamos a utilizar la clase Perro en el espacio clases/animales/Perro</p>
</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
// autocarga de clases
// cada vez que instancie un objeto (new ...)
// llama a esta funcion
// como utilizo espacios de nombres no coloco los directorios delante de la clase


// importo la clase perro


// creo un nuevo perro
// lo llamo Tor, Pastor aleman

// muestro informacion sobre el perro creado con var_dump


// imprimo el perro1


// mostrar un perro

// mostrar un perro


// creo un nuevo coche
// al no tener importada la clase necesito indicarle el espacio de nombres


// muestro informacion sobre el coche creado con var_dump

    </pre>
</div>

<div style="background-color: #ccc;margin:20px auto;width:900px;padding:20px">
    <pre>
// autocarga de clases
// cada vez que instancie un objeto (new ...)
// llama a esta funcion
// como utilizo espacios de nombres no coloco los directorios delante de la clase
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

// importo la clase perro
use clases\animales\Perro;

// creo un nuevo perro
// lo llamo Tor, Pastor aleman
$perro1 = new Perro("Tor", "Pastor aleman");

// muestro informacion sobre el perro creado con var_dump
var_dump($perro1);

// imprimo el perro1
echo $perro1;

// mostrar un perro
echo $perro1->mostrar();
// mostrar un perro
echo $perro1->mostrar();

// creo un nuevo coche
// al no tener importada la clase necesito indicarle el espacio de nombres
$coche1 = new clases\objetos\Coche("2341mmm");
// muestro informacion sobre el coche creado con var_dump
var_dump($coche1);
    </pre>
</div>

<?php
// autocarga de clases
// cada vez que instancie un objeto (new ...)
// llama a esta funcion
// como utilizo espacios de nombres no coloco los directorios delante de la clase
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

// importo la clase perro
use clases\animales\Perro;

// creo un nuevo perro
// lo llamo Tor, Pastor aleman
$perro1 = new Perro("Tor", "Pastor aleman");
// muestro informacion sobre el perro creado con var_dump
var_dump($perro1);

// imprimo el perro1
echo $perro1;

// mostrar un perro
echo $perro1->mostrar();
// mostrar un perro
echo $perro1->mostrar();

// creo un nuevo coche
// al no tener importada la clase necesito indicarle el espacio de nombres
$coche1 = new clases\objetos\Coche("2341mmm");
// muestro informacion sobre el coche creado con var_dump
var_dump($coche1);
