<?php

use clases\dos\Caja as DosCaja;
use clases\uno\Caja;
use clases\uno\Lista;

// Autocarga de clases
spl_autoload_register(function ($nombreClase) {
    require_once "$nombreClase.php";
});

$titulo = new Caja();

$lateral = new DosCaja();

var_dump($titulo);
var_dump($lateral);

// Crear un objeto de tipo lista

$listado = new Lista();

var_dump($listado);
